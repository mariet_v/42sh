/*
** exec_line.c for 42sh in /home/chiny_a//42sh
**
** Made by antoine chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Wed May  8 14:51:45 2013 antoine chiny
   Last update Fri May 24 18:46:59 2013 valentin mariette
*/

#include "mysh.h"

t_sep	**init_sep(t_shell *shell)
{
  int	i;
  char	**cs;
  t_sep **sep;

  if (!(cs = my_wordtab(shell->line, ";")))
    return (NULL);
  if (!(sep = malloc(sizeof(t_sep *) * (tablen(cs) + 1))))
    return (NULL);
  i = -1;
  while (cs[++i])
    {
      if (!(sep[i] = malloc(sizeof(t_sep))))
	return (NULL);
      if (!(cs[i] = epur_str(cs[i], " \t")))
	return (NULL);
      if (!(sep[i]->cmd = my_wordtab(cs[i], " \t")))
	return (NULL);
      if (!(sep[i]->type = get_lexem(shell, sep[i]->cmd)))
	return (NULL);
    }
  if (!(sep[i] = malloc(sizeof(t_sep))))
    return (NULL);
  sep[i] = NULL;
  return (sep);
}

int	exec_line(t_shell *shell, char **env)
{
  int	i;

  if ((shell->sep = init_sep(shell)) == NULL)
    return (MALLOC_ERROR);
  i = -1;
  if (shell->line[0])
    while (shell->sep[++i])
      {
	if (pipe_part(shell, shell->sep[i], env, i) == -1)
	  return (-1);	
      }
  return (EXIT_SUCESS);
}
