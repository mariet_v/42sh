/*
** get_concatenate_path.c for mysh in /home/mariet_v//celem/mysh1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Dec 17 10:55:42 2012 valentin mariette
** Last update Fri May 24 18:48:28 2013 valentin mariette
*/

#include "mysh.h"

char		*get_concatenate_path(char *cmd, char *path_whole)
{
  DIR		*path_dir;
  int		i;
  int		j;
  char		*path;
  struct dirent	*ptr_file;

  i = 5;
  j = 0;
  while (i < my_strlen(path_whole) && j < my_strlen(path_whole))
    {
      j = i + 1;
      while (path_whole[j] != ':' && path_whole[j] != '\0')
	j++;
      path = my_strnndup(path_whole, i, j - 1);
      path_dir = opendir(path);
      if (path_dir == NULL)
	return (NULL);
      ptr_file = readdir(path_dir);
      while (ptr_file != NULL && my_strcmp(cmd, ptr_file->d_name) != 0)
	ptr_file = readdir(path_dir);
      if (ptr_file != NULL && my_strcmp(cmd, ptr_file->d_name) == 0)
	return (my_concatenate(path, cmd));
      i = j + 1;
    }
  return (NULL);
}

char	*my_concatenate(char *str1, char *str2)
{
  char	*str_f;
  int	i;
  int	j;

  i = 0;
  j = 0;
  str_f = malloc(((my_strlen(str1) + my_strlen(str2)) + 2) * sizeof(char));
  while (str1[i] != '\0')
    {
      str_f[i] = str1[i];
      i++;
    }
  str_f[i++] = '/';
  while (str2[j] != '\0')
    {
      str_f[i + j] = str2[j];
      j++;
    }
  str_f[i + j] = '\0';
  return (str_f);
}
