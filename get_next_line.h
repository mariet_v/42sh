/*
** get_next_line.h for get_next_line in /home/mariet_v//celem/gnl
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Nov 19 11:17:58 2012 valentin mariette
** Last update Wed May 22 16:32:43 2013 valentin mariette
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

#include "mysh.h"

# define MAX_LEN_BUFFER 4096
# define MAX_LEN_READ   4096


char    *case_too_small(char *buf, int fd, int len, int r_v);
char             *get_next_line(const int fd);
char    *case_big_enough(char *string, char *buffer);

#endif
