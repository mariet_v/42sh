/*
** my_wordtab.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May  2 16:21:30 2013 Antoine Chiny
** Last update Thu May 23 00:58:44 2013 jonathan collinet
*/

#include "mysh.h"

int	count_word(char *str, char *sep)
{
  int	i;
  int	n;
  int	t;

  i = 0;
  n = 0;
  t = 0;
  if (!str[0])
    return (0);
  while (str[i])
    {
      if (!is_separator(str[i], sep) && t)
	{
	  t = 0;
	  n++;
	}
      if (!is_separator(str[i], sep))
	t = 1;
      i++;
    }
  if (is_separator(str[strlen(str) - 1], sep))
    return (n);
  else
    return (n + 1);
}

int	count_char(char *str, char *sep, int index)
{
  int	n;

  n = 0;
  while (!is_separator(str[index], sep) && str[index])
    {
      n++;
      index++;
    }
  return (n);
}

char	**my_wordtab(char *str, char *sep)
{
  int	i;
  int	x;
  int	y;
  char	**t;

  y = 0;
  i = 0;
  if (!str)
    return (NULL);
  if (!(t = malloc(sizeof(char *) * (count_word(str, sep) + 1))))
    return (NULL);
  while (str[i])
    {
      x = 0;
      if (!is_separator(str[i], sep))
	{
	  if (!(t[y] = malloc(sizeof(char) * (count_char(str, sep, i) + 1))))
	    return (NULL);
	  while (!is_separator(str[i], sep) && str[i])
	    t[y][x++] = str[i++];
	  t[y++][x] = '\0';
	}
      else
	i++;
    }
  t[y] = '\0';
  return (t);
}
