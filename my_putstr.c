/*
** my_putstr.c for 42sh in /home/mariet_v//celem/42sh/42sh
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu May  9 17:00:41 2013 valentin mariette
** Last update Fri May 24 18:49:53 2013 valentin mariette
*/

#include "mysh.h"

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    my_putchar(str[i++]);
}
