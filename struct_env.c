/*
** sturct_env.c for 42sh in /home/chiny_a/SVN/42sh-2017-chiny_a
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May 16 15:03:23 2013 Antoine Chiny
** Last update Thu May 16 18:49:25 2013 jonathan collinet
*/

#include "mysh.h"

int	check_env(char *name, t_env **env)
{
  int	i;

  i = -1;
  while (env[++i])
    if (!my_strcmp(name, env[i]->name))
      return (0);
  return (-1);
}
