/*
** handle_shell.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May  2 14:51:07 2013 Antoine Chiny
** Last update Fri May 24 17:18:52 2013 valentin mariette
*/

#include "mysh.h"

int	put_prompt(t_shell *shell, char *prompt)
{
  char	*pwd;
  int	pos;

  if (!(prompt = get_env_instruct("USERNAME", shell->env)))
    prompt = "|unknow user|";
  if ((pos = get_pos_env("PWD", shell->env)) < 0)
    pwd = "|unknow path|";
  else
    pwd = my_strdup(shell->env[pos]->value);
  if ((write(1, prompt, strlen(prompt))) == -1)
    return (-1);
  if ((write(1, " ", 1)) == -1)
    return (-1);
  if ((write(1, pwd, strlen(pwd))) == -1)
    return (-1);
  if ((write(1, " $> ", strlen(" $> "))) == -1)
    return (-1);
  return (0);
}

t_env	**init_env(char **env)
{
  int	i;
  char	**line;
  t_env	**s_env;

  i = -1;
  if (!(s_env = malloc(sizeof(t_env *) * (tablen(env) + 1))))
    return (NULL);
  while (env[++i])
    {
      line = my_wordtab(env[i], "=");
      if (!(s_env[i] = malloc(sizeof(t_env))))
	return (NULL);
      if (!(s_env[i]->name = my_strdup(line[0])))
	return (NULL);
      if (tablen(line) == 2)
	{
	  if (!(s_env[i]->value = my_strdup(line[1])))
	    return (NULL);
	}
      else
	s_env[i]->value = NULL;
      free_tab(line);
    }
  if (!(s_env[i] = malloc(sizeof(t_env))))
    return (NULL);
  s_env[i] = NULL;
  return (s_env);
}

int	init_shell(t_shell *shell, char **env)
{
  shell->b_funct = malloc(sizeof(t_builtins *) * (NB_BUILTINS + 1));
  shell->b_funct[0] = malloc(sizeof(t_builtins));
  shell->b_funct[0]->funct = my_setenv;
  shell->b_funct[0]->name = "setenv";
  shell->b_funct[1] = malloc(sizeof(t_builtins));
  shell->b_funct[1]->funct = my_unsetenv;
  shell->b_funct[1]->name = "unsetenv";
  shell->b_funct[2] = malloc(sizeof(t_builtins));
  shell->b_funct[2]->funct = my_cd;
  shell->b_funct[2]->name = "cd";
  shell->b_funct[3] = malloc(sizeof(t_builtins));
  shell->b_funct[3]->funct = my_env;
  shell->b_funct[3]->name = "env";
  shell->b_funct[4] = malloc(sizeof(t_builtins));
  shell->b_funct[4]->funct = my_exit;
  shell->b_funct[4]->name = "exit";
  shell->b_funct[5] = malloc(sizeof(t_builtins));
  shell->b_funct[5]->funct = echo_this;
  shell->b_funct[5]->name = "echo";
  shell->b_funct[6] = malloc(sizeof(t_builtins));
  shell->b_funct[6] = NULL;
  if (!(shell->env = init_env(env)))
    return (MALLOC_ERROR);
  return (0);
}

char	*get_line(char *buff, int n)
{
  char	*line;

  buff[n - 1] = '\0';
  if (!(line = epur_str(buff, " \t")))
    return (NULL);
  return (line);
}

int		handle_shell(char **env)
{
  char		buff[READ];
  t_shell	shell;
  int		n;
  int		err;

  n = 1;
  if ((init_shell(&shell, env)))
    return (MALLOC_ERROR);
  while (1)
    {
      if (n)
	if (put_prompt(&shell, NULL) == -1)
	  return (WRITE_ERROR);
      if ((n = read(0, buff, READ)) == -1)
  	return (READ_ERROR);
      if (!(shell.line = get_line(buff, n)))
	return (MALLOC_ERROR);
      if ((err = exec_line(&shell, env)) < 0)
	return (return_error(1, "Program exited."));
    }
  free_env(shell.env);
  return (EXIT_SUCESS);
}
