/*
** edit_cmdline.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May  2 15:41:22 2013 Antoine Chiny
** Last update Mon May 20 16:48:29 2013 jonathan collinet
*/

#include "mysh.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

int	is_separator(char c, char *sep)
{
  int	i;

  i = -1;
  while (sep[++i])
    if (sep[i] == c)
      return (1);
  return (0);
}

int	line_len(char *line, char *sep)
{
  int	i;
  int	len;

  i = 0;
  len = 0;
  while (is_separator(line[i], sep) && line[i])
    i++;
  while (line[i])
    {
      len++;
      if (is_separator(line[i], sep))
	while (is_separator(line[i], sep) && line[i])
	  i++;
      else
	i++;
    }
  return (len);
}

char	*epur_str(char *cmdline, char *sep)
{
  int	i;
  int	j;
  char	*line;

  i = 0;
  j = 0;
  line = NULL;
  if (!(line = malloc(sizeof(char) * (line_len(cmdline, sep) + 1))))
    return (NULL);
  while (is_separator(cmdline[i], sep) && cmdline[i])
    i++;
  while (cmdline[i])
    {
      if (is_separator(cmdline[i], sep))
	{
	  while (is_separator(cmdline[i], sep) && cmdline[i])
	    i++;
	  if (cmdline[i])
	    line[j++] = ' ';
	  else
	    {
	      line[j] = '\0';
	      return (line);
	    }
	}
      line[j++] = cmdline[i];
      i++;
    }
  line[j] = '\0';
  return (line);
}
