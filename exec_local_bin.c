/*
** exec_local_bin.c for mysh in /home/mariet_v//celem/mysh1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Wed Dec 19 11:54:35 2012 valentin mariette
** Last update Fri May 24 18:47:25 2013 valentin mariette
*/

#include "mysh.h"

void	exec_local_bin(char **foo, char **env)
{
  execve(foo[0], foo, env);
}
