/*
** echo_this.c for 42sh in /home/collin_b//42/42sh_SAVE
**
** Made by jonathan collinet
** Login   <collin_b@epitech.net>
**
** Started on  Thu May 16 20:58:10 2013 jonathan collinet
** Last update Fri May 24 16:29:41 2013 jonathan collinet
*/

#include "mysh.h"

int	echo_this(t_shell *shell, char **cmd)
{
  if (!shell)
    return (0);
  if (tablen(cmd) > 2)
    return (return_error(0, "Echo cannot take more than one arg."));
  if (cmd[1])
    printf("%s\n", cmd[1]);
  else
    printf("%s : put an argument\n", cmd[0]);
  return (0);
}
