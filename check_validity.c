/*
** check_validity.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Tue May  7 16:02:57 2013 Antoine Chiny
** Last update Tue May 21 17:23:04 2013 jonathan collinet
*/

#include "mysh.h"

int	check_command(int *lexem, int i, int len)
{
  if (i < len && lexem[i + 1] == ARGUMENT)
    while (lexem[++i] == ARGUMENT && i < len)
      lexem[i] = COM_ARGS;
  return (0);
}

int	check_pipe(int *lexem, int i, int len)
{
  if (!i || i == (len - 1) || lexem[i + 1] == OPE_PIPE)
    return (1);
  else
    return (0);
}

int	check_validity(int *lexem, int len)
{
  int	i;

  i = -1;
  while (++i < len)
    {
      if (lexem[i] == COMMAND ||
	  lexem[i] == BINARY)
	check_command(lexem, i, len);
      if (lexem[i] == OPE_PIPE)
	if (check_pipe(lexem, i, len))
	  return (SYNTAX_ERROR);
    }
  return (0);
}
