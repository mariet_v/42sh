/*
** in_out.c for 42sh in /home/mariet_v//celem/42sh/42sh-2017-chiny_a/my_test
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Wed May  8 17:59:31 2013 valentin mariette
** Last update Sat May 25 14:39:30 2013 valentin mariette
*/

#include "mysh.h"
#include "get_next_line.h"

int	make_in(t_listecmd *liste, t_sep *shell, int i)
{
  char	*buff;
  char	*str;

  if (i > 0 && !my_strncmp(shell->cmd[i - 1], "<<", 2))
    {
      str = my_strdup("");
      my_putstr("? ");
      while (my_strcmp(shell->cmd[i], buff = get_next_line(0)))
	{
	  my_putstr("? ");
	  str = concatenate(str, buff, '\n');
	}
      liste->buff = str;
      if (double_redir_pipe(liste) == -1)
	return (-1);
    }
  else
    {
      liste->in = open(shell->cmd[i], O_RDONLY);
      if (liste->in < 0)
	{
	  printf("File not found or can't be opened\n");
	  return (-1);
	}
      return (0);
    }
  return (0);
}

int	make_out(t_listecmd *liste, t_sep *shell, int i)
{
  int	flag;

  flag = O_WRONLY | O_CREAT;
  if (i > 0)
    {
      if(!my_strncmp(shell->cmd[i - 1], ">>", 2))
	flag |= O_APPEND;
      else
	flag |= O_TRUNC;
    }
  liste->out = open(shell->cmd[i],  flag, 0644);
  if (liste->out < 0)
    {
      printf("File can't be opened or created\n");
      return (-1);
    }
  return (0);
}

int	double_redir_pipe(t_listecmd *liste)
{
  int	fd[2];

  if (pipe(fd) == -1)
    return (-1);
  liste->double_out = fd[0];
  liste->double_in = fd[1];
  return (0);
}
