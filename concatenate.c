/*
** concatenate.c for 42sh in /home/mariet_v//celem/42sh/42sh-2017-chiny_a/my_test
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Wed May  8 17:02:26 2013 valentin mariette
** Last update Fri May 24 18:46:22 2013 valentin mariette
*/

#include "mysh.h"

char    *concatenate(char *str1, char *str2, char sep)
{
  char  *str_f;
  int   i;
  int   j;

  i = 0;
  j = 0;
  str_f = malloc(((my_strlen(str1) + my_strlen(str2)) + 2) * sizeof(char));
  while (str1[i] != '\0')
    {
      str_f[i] = str1[i];
      i++;
    }
  str_f[i++] = sep;
  while (str2[j] != '\0')
    {
      str_f[i + j] = str2[j];
      j++;
    }
  str_f[i + j] = '\0';
  free(str1);
  return (str_f);
}
