/*
** environ.c for 42sh in /home/chiny_a//42sh
**
** Made by antoine chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Wed May  8 14:18:10 2013 antoine chiny
** Last update Thu May 23 00:21:20 2013 jonathan collinet
*/

#include "mysh.h"

void	display_env(t_env **env)
{
  int	i;

  i = -1;
  if (!env)
    printf("Env is empty.");
  while (env[++i])
    {
      if (env[i]->value)
	printf("%s=%s\n", env[i]->name, env[i]->value);
      else
	printf("%s=\n", env[i]->name);
    }
  if (!i)
    printf("Env is empty.");
}

void	free_env(t_env **env)
{
  int	i;

  i = -1;
  if (!env)
    return ;
  while (env[++i])
    {
      free(env[i]->name);
      if (env[i]->value)
	free(env[i]->value);
      free(env[i]);
    }
  free(env);
}

char	*get_envline(t_env *env)
{
  int	i;
  int	j;
  char	*line;

  i = -1;
  j = -1;
  if (!env)
    return (NULL);
  if (!(line = malloc(sizeof(char)
		      * (my_strlen(env->name) + my_strlen(env->value) + 2))))
    return (NULL);
  while (env->name[++i])
    line[i] = env->name[i];
  line[i] = '=';
  if (env->value)
    {
      while (env->value[++j])
	line[++i] = env->value[j];
    }
  line[++i] = '\0';
  return (line);
}

char	**get_char_env(t_env **env)
{
  int	i;
  int	len;
  char	**c_env;

  i = 0;
  len = 0;
  if (!env)
    return (NULL);
  while (env[i])
    {
      i++;
      len++;
    }
  i = -1;
  if (!(c_env = malloc(sizeof(char *) * (len + 1))))
    return (NULL);
  while (env[++i])
    c_env[i] = get_envline(env[i]);
  c_env[i] = '\0';
  return (c_env);
}

char	*get_struct_env(char *name, t_env **env)
{
  int	i;

  i = -1;
  if (!env)
    return (NULL);
  while (env[++i])
    if (!my_strcmp(name, env[i]->name))
      return (env[i]->value);
  return (NULL);
}
