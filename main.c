/*
** main.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Wed May  1 18:41:25 2013 Antoine Chiny
** Last update Fri May 24 19:38:44 2013 stephen oudot
*/

#include "mysh.h"

void	handle_signal(int signum)
{
  signal(signum, &handle_signal);
  write(1, "\n", 1);
}

int	main(int ac, char *av[], char **env)
{
  int	e;
  char	**m_tab;

  if (!(m_tab = init_error()))
    {
      printf("%s", INIT_ERROR);
      return (-1);
    }
  if (ac == 1)
    {
      signal(SIGINT, &handle_signal);
      if (env[0])
	if ((e = handle_shell(env)) < 0)
	  {
	    check_error(e, m_tab);
	    free_tab(m_tab);
	    return (-1);
	  }
    }
  else
    fprintf(stderr, "%s don't need arguments.\n", av[0]);
  return (EXIT_SUCESS);
}
