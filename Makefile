##
## Makefile for 42sh in /home/chiny_a/42sh
## 
## Made by Antoine Chiny
## Login   <chiny_a@epitech.net>
## 
## Started on  Thu May  2 14:29:32 2013 Antoine Chiny
## Last update Fri May 24 19:23:09 2013 stephen oudot
##

SRC	= main.c\
	cd.c \
	pipe.c\
	error.c\
	in_out.c\
	environ.c\
	builtins.c\
	env_funct.c\
	exec_line.c\
	echo_this.c\
	make_pipe.c\
	my_putstr.c\
	str_funct.c\
	cmp_funct.c\
	tab_funct.c\
	get_lexem.c\
	exec_funct.c\
	my_wordtab.c\
	path_funct.c\
	struct_env.c\
	look_status.c\
	my_strnndup.c\
	concatenate.c\
	forkdupexec.c\
	check_funct.c\
	handle_shell.c\
	edit_cmdline.c\
	get_next_line.c\
	exec_builtins.c\
	check_validity.c\
	exec_local_bin.c\
	put_in_arg_tab.c\
	get_concatenate_path.c\

OBJ	= $(SRC:.c=.o)

CC	= gcc -g

CFLAGS	= -Wall -Wextra -ansi

RM	= rm -f

NAME	= 42sh

$(NAME):	$(OBJ)
	$(CC) -o $(NAME) $(OBJ)

all:	$(NAME)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all