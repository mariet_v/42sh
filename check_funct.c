/*
** check_funct.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sun May  5 19:21:24 2013 Antoine Chiny
** Last update Tue May 21 17:05:38 2013 jonathan collinet
*/

#include "mysh.h"

int	is_and_ope(char *name)
{
  int	i;
  int	bool;
  char	**ope_and;

  i = -1;
  bool = 1;
  if (!(ope_and = my_wordtab(AND, ":")))
    return (MALLOC_ERROR);
  while (ope_and[++i] && bool)
    if (!my_strcmp(ope_and[i], name))
      bool = 0;
  free_tab(ope_and);
  return (bool);
}

int	is_redirection(char *name)
{
  int	i;
  int	bool;
  char	**pipes;

  i = -1;
  bool = 1;
  if (!(pipes = my_wordtab(PIPE, ":")))
    return (MALLOC_ERROR);
  while (pipes[++i] && bool)
    if (!my_strcmp(pipes[i], name))
      bool = 0;
  free_tab(pipes);
  return (bool);
}

int	is_binary(char *name)
{
  if (!my_strncmp(name, "./", 2))
    if (!access(name, X_OK))
      return (0);
  return (-1);
}

int	is_command(char *name, char **env)
{
  int	i;
  int	r;
  char	*str;
  char	*path;
  char	**tab;

  i = -1;
  r = 1;
  if ((path = get_env("PATH", env)) == NULL)
    return (ENV_ERROR);
  if ((tab = my_wordtab(path, ":")) == NULL)
    return (MALLOC_ERROR);
  while (tab[++i])
    {
      if ((str = my_strcat_path(tab[i], name)) == NULL)
	return (MALLOC_ERROR);
      if (!access(str, X_OK))
	r = 0;
      free(str);
    }
  free_tab(tab);
  return (r);
}

int	is_builtins(t_builtins **b, char *name)
{
  int	i;

  i = -1;
  while (++i < NB_BUILTINS - 1)
    {
      if (!my_strcmp(b[i]->name, name))
	return (0);
    }
  return (1);
}
