/*
** pipe.c for 42sh in /home/mariet_v//celem/42sh/42sh-2017-chiny_a/my_test
**
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
**
** Started on  Wed May  8 16:09:55 2013 valentin mariette
** Last update Fri May 24 19:30:31 2013 stephen oudot
*/

#include <stdlib.h>
#include "mysh.h"

int		pipe_part(t_shell *shell, t_sep *sep, char **env, int i)
{
  t_listecmd	*list;

  list = NULL;
  if (there_is_pipe(sep))
    {
      if (put_in_listecmd(sep, &list) == -1)
	return (MALLOC_ERROR);
      if (!list)
	{
	  fprintf(stderr, "%s", INVALID_ARG);
	  return (42);
	}
      make_pipe_between(list);
      forkdupexec(list, env);
    }
  else
    {
      if (shell->sep[i]->type[0] == COMMAND)
	{
	  if ((exec_cmd(0, shell, shell->sep[i])) == -1)
	    printf("Command not found\n");
	}
      else if (shell->sep[i]->type[0] == BUILTINS)
	{
	  if ((exec_builtins(0, shell, shell->sep[i])) == -1)
	    return (-1);
	  else if (i == -42)
	    return (-42);
	}
    }
  return (0);
}


int	there_is_pipe(t_sep *shell)
{
  int	i;

  i = -1;
  while (shell->cmd[++i])
    {
      if (shell->type[i] == OPE_PIPE)
	return (1);
    }
  return (0);
}

int	put_in_listecmd(t_sep *shell, t_listecmd **liste)
{
  int	i;
  int	num_cmd;
  t_listecmd *tmp;

  i = -1;
  num_cmd = 0;
  while (shell->cmd[++i])
    {
      if (shell->type[i] == COMMAND && *liste == NULL)
	{
	  if (!(tmp = malloc(sizeof(t_listecmd))))
	    return (MALLOC_ERROR);
	  *liste = tmp;
	  tmp->next = NULL;
	  tmp->previous = NULL;
	  tmp->buff = NULL;
	  tmp->cmd = whole_cmd(shell, i);
	  find_inout(tmp, shell, num_cmd);
	}
      else if (shell->type[i] == COMMAND)
	{
	  if (get_new_elem(*liste, shell, num_cmd) != 0)
	    return (MALLOC_ERROR);
	}
      else if (my_strncmp(shell->cmd[i], "|", 1) == 0)
	num_cmd++;
    }
  return (0);
}

int		get_new_elem(t_listecmd *tmp, t_sep *shell, int num_cmd)
{
  t_listecmd	*new;
  int		i;
  int		num_pipe;

  i = 0;
  num_pipe = 0;
  while (num_pipe < num_cmd && shell->cmd[++i])
    if (my_strncmp(shell->cmd[i], "|", 1) == 0)
      num_pipe++;
  i++;
  if (!(new = malloc(sizeof(*new))))
    return (MALLOC_ERROR);
  while (tmp->next != NULL)
    tmp = tmp->next;
  new->cmd = whole_cmd(shell, i);
  tmp->next = new;
  new->previous = tmp;
  new->next = NULL;
  new->buff = NULL;
  find_inout(new, shell, num_cmd);
  return (0);
}

char	*whole_cmd(t_sep *shell, int i)
{
  char	*str;

  str = my_strdup(shell->cmd[i]);
  while (shell->type[++i] == COM_ARGS || shell->type[i] == ARGUMENT)
    str = concatenate(str, shell->cmd[i], ' ');
  return (str);
}

int	find_inout(t_listecmd *liste, t_sep *shell, int num_cmd)
{
  int	i;
  int	num_pipe;

  i = 0;
  num_pipe = 0;
  liste->in = 0;
  liste->out = 1;
  while (num_pipe < num_cmd && shell->cmd[++i])
    if (my_strncmp(shell->cmd[i], "|", 1) == 0)
      num_pipe++;
  i++;
  while (shell->cmd[i] && my_strncmp(shell->cmd[i], "|", 1) != 0)
    {
      if (my_strncmp(shell->cmd[i], ">", 1) == 0)
	make_out(liste, shell, i + 1);
      if (my_strncmp(shell->cmd[i], "<", 1) == 0)
	make_in(liste, shell, i + 1);
      i++;
    }
  return (1);
}
