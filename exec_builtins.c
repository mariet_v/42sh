/*
** exec_builtins.c for 42sh in /home/collin_b//42/42sh_SAVE
**
** Made by jonathan collinet
** Login   <collin_b@epitech.net>
**
** Started on  Thu May 16 20:19:11 2013 jonathan collinet
** Last update Mon May 20 15:44:20 2013 jonathan collinet
*/

#include "mysh.h"

int	exec_builtins(int id, t_shell *shell, t_sep *sep)
{
  int	i;

  i = -1;
  while (shell->b_funct[++i])
    if (!my_strcmp(shell->b_funct[i]->name, sep->cmd[id]))
      return (shell->b_funct[i]->funct(shell, sep->cmd));
  return (tablen(sep->cmd));
}
