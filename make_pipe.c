/*
** make_pipe.c for 42sh in /home/mariet_v//celem/42sh/42sh
**
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
**
** Started on  Fri May 17 18:33:20 2013 valentin mariette
** Last update Fri May 24 18:58:51 2013 stephen oudot
*/
#include <stdlib.h>
#include "mysh.h"

int	make_pipe_between(t_listecmd *liste)
{
  t_listecmd    *first;
  t_listecmd    *second;
  int           pipefd[2];

  first = liste;
  second = first->next;
  while (second != NULL)
    {
      if (pipe(pipefd) == -1)
        return (-1);
      first->out = pipefd[1];
      second->in = pipefd[0];
      first = first->next;
      second = second->next;
    }
  return (0);
}
