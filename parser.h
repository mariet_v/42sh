/*
** parser.h for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Tue May  7 15:34:46 2013 Antoine Chiny
** Last update Tue May 21 17:24:18 2013 jonathan collinet
*/

#ifndef PARSER_H_
# define PARSER_H_

# define OPE_PIPE	1
# define OPE_AND	2
# define COMMAND	3
# define COM_ARGS	4
# define ARGUMENT	5
# define BUILTINS	6
# define BINARY		7

#endif /* PARSER_H_ */
