/*
** cd.c for 42sh in /home/collin_b//42sh-2017-chiny_a
**
** Made by jonathan collinet
** Login   <collin_b@epitech.net>
**
** Started on  Thu May  9 16:07:18 2013 jonathan collinet
** Last update Fri May 24 22:56:05 2013 jonathan collinet
*/

#include "mysh.h"

char	**reform_tab(char **cur_path)
{
  int	i;
  int	j;
  char	**pwd;

  if (!cur_path[0] || !cur_path[0][0])
    return (NULL);
  if (tablen(cur_path) == 1 && !my_strcmp(cur_path[0], ".."))
    {
      if (!my_strcmp(cur_path[0], ".."))
	printf("cd : wrong path\n");
      return (NULL);
    }
  if (!(pwd = malloc(sizeof(char *) * (tablen(cur_path) + 1))))
    return (NULL);
  i = -1;
  j = -1;
  while (cur_path[++i])
    if (!my_strcmp(cur_path[i], ".") || !my_strcmp(cur_path[i], ".."))
      j--;
    else
      pwd[++j] = "";
  pwd[++j] = NULL;
  return (pwd);
}

char	*reform_pwd(char **cur_path)
{
  int	i;
  char	*pwd;

  i = -1;
  pwd = NULL;
  if (cur_path)
    {
      if (!my_strcmp(cur_path[0], ".."))
	return (NULL);
      else
	while (cur_path[++i])
	  pwd = my_strcat_path(pwd, cur_path[i]);
    }
  return (pwd);
}

int	old_pwd(t_shell *shell, int pos)
{
  char	*tmp;

  if (!shell->old_pwd)
    if (!(shell->old_pwd = get_env_instruct("PWD",  shell->env)))
      return (return_error(0, "cd - : PWD isn't in env."));
  tmp = shell->env[pos]->value;
  shell->env[pos]->value = my_strdup(shell->old_pwd);
  go_here(shell->env, shell->old_pwd);
  shell->old_pwd = tmp;
  return (0);
}

int	go_here(t_env **env, char *pwd)
{
  if (!my_strncmp(pwd, "/..", 3))
    return (0);
  if (!pwd)
    pwd = my_strdup("/");
  if (access(pwd, F_OK) == -1)
    return (return_error(0, "cd : the path don't exist."));
  if (chdir(pwd) == -1)
    return (return_error(0, "cd : the path exist but can't access."));
  env[get_pos_env("PWD", env)]->value = my_strdup(pwd);
  return (0);
}

int	build_new_pwd(t_shell *shell, char **cur_path, char **cmd_path, int i)
{
  int	j;
  char	*pwd;

  j = tablen(cur_path);
  pwd = NULL;
  shell->old_pwd = my_strdup(get_env_instruct("PWD", shell->env));
  if (!cur_path)
    if (!(cur_path = reform_tab(cmd_path)))
      return (go_here(shell->env, "/"));
  while (cmd_path[++i])
    if (!my_strcmp(cmd_path[i], "."));
    else if (!my_strcmp(cmd_path[i], "..") && tablen(cur_path))
      {
	cur_path[tablen(cur_path) - 1] = NULL;
	j--;
      }
    else
      cur_path[j++] = my_strdup(cmd_path[i]);
  if (!(pwd = reform_pwd(cur_path)))
    return (0);
  return (go_here(shell->env, pwd));
}
