/*
** mysh.h for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May  2 14:31:02 2013 Antoine Chiny
** Last update Sat May 25 14:40:07 2013 valentin mariette
*/

#ifndef MYSH_H_
# define MYSH_H_

# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <signal.h>
# include "error.h"
# include "parser.h"

# define AND		"&&:||:;"
# define PIPE		"|:<:<<:>>:>"
# define READ		1096
# define ABS(x)		((x) < (0) ? (-x) : (x))
# define BZ(x)		((x) < (0) ? (0) : (x))
# define NB_BUILTINS	7

struct		s_shell;
struct		s_sep;

typedef struct	s_env
{
  char		*name;
  char		*value;
}		t_env;

typedef struct	s_builtins
{
  int		(*funct)(struct s_shell *, char **);
  char		*name;
}		t_builtins;

typedef struct	l_sep
{
  char		**cmd;
  int		*type;
}		t_sep;

typedef struct	s_shell
{
  char		*line;
  char		*old_pwd;
  t_env		**env;
  t_builtins	**b_funct;
  t_sep		**sep;
}		t_shell;

typedef struct		s_listecmd
{
  char			*cmd;
  int			in;
  int			out;
  int			double_in;
  int			double_out;
  struct s_listecmd	*previous;
  struct s_listecmd	*next;
  int			is_cmd;
  char			*buff;
}			t_listecmd;

/*
**	env_funct.c
*/

int	empty_env(t_shell *shell, char *option);
int	len_env(t_env **env);
int	cp_env(t_env **env, t_shell *shell);
int	del_var_in_env(t_shell *shell, char *name);
int	add_var_in_env(t_shell *shell, char *name, char *value);
void     handle_signal(int signum);
/*
**	utils.c
*/

void	aff_liste(t_listecmd *liste);

/*
**	concatenate.c
*/

char	*concatenate(char *str1, char *str2, char sep);

/*
**	exec_local_bin.c
*/

void	exec_local_bin(char **foo, char **env);

/*
**	forkdupexec.c
*/

int		forkdupexec(t_listecmd *liste, char **env);
t_listecmd      *get_next(t_listecmd *tmp);
char		*my_check_env(char **env);
char		**arg_tab_double_left(char *cmd);

/*
**	get_concatenate_path.c
*/

char	*get_concatenate_path(char *cmd, char *path_whole);
char    *my_concatenate(char *str1, char *str2);

/*
**	look_status.c
*/

void	look_status(int status);

/*
**	make_pipe.c
*/

int	make_pipe_between(t_listecmd *liste);

/*
**	my_strnndup.c
*/

char	*my_strnndup(char *src, int first, int last);

/*
**	pipe.c
*/

int	pipe_part(t_shell *shell, t_sep *sep, char **env, int i);
int	there_is_pipe(t_sep *shell);
int	put_in_listecmd(t_sep *shell, t_listecmd **liste);
int	get_new_elem(t_listecmd *tmp, t_sep *shell, int num_cmd);
char	*whole_cmd(t_sep *shell, int i);

/*
**	put_in_arg_tab.c
*/

char	**put_in_arg_tab(char *cmd);

/*
**	my_put_nbr.c
*/

void	my_putchar(char c);
void	my_putstr(char *str);
int	puissance(int a, int b);
int	get_nbr_char(int nbr);
int	my_put_nbr(int nbr);

/*
**	int_out.c
*/

int	make_in(t_listecmd *liste, t_sep *shell, int i);
int	make_out(t_listecmd *liste, t_sep *shell, int i);
int	find_inout(t_listecmd *liste, t_sep *shell, int num_cmd);
int	double_redir_pipe(t_listecmd *liste);

/*
**	echo_this.c
*/

int	echo_this(t_shell *shell, char **cmd);

/*
**	cd.c
*/

int	old_pwd(t_shell *shell, int pos);
char	*reform_pwd(char **cur_path);
int	build_new_pwd(t_shell *shell, char **cur_path, char **cmd_path, int i);
int	go_here(t_env **env,char *pwd);

/*
**	exec_func.c
*/

char	*get_exec_path(char *cmd, int type, t_shell *shell, int *e);
int	get_nbr_args(int id, t_sep *sep);
char	**get_cmd_args(int id, t_sep *sep);
int	exec_path_cmd(char *cmd, char **args, char **env);
int	exec_cmd(int id, t_shell *shell, t_sep *sep);

/*
**	exec_builtins.c
*/

int	exec_builtins(int id, t_shell *shell, t_sep *sep);

/*
**	builtins.c
*/

int	my_setenv(t_shell *shell, char **cmd);
int	my_unsetenv(t_shell *shell, char **cmd);
int	my_cd(t_shell *shell, char **cmd);
int	my_env(t_shell *shell, char **cmd);
int	my_exit(t_shell *shell, char **cmd);

/*
**	edit_cmdline.c
*/

void	my_putchar(char c);
int	is_separator(char c, char *sep);
int	line_len(char *line, char *sep);
char	*epur_str(char *cmdline, char *sep);

/*
**	my_wordtab.c
*/

int	count_word(char *str, char *sep);
int	count_char(char *str, char *sep, int index);
char	**my_wordtab(char *str, char *sep);

/*
**	tab_funct.c
*/

int	tablen(char **t);
int	display_tab(char **t);
void	free_tab(char **t);

/*
**	cmp_funct.c
*/

int	my_strncmp(char *s1, char *s2, int n);
int	my_strcmp(char *s1, char *s2);

/*
**	path_funct.c
*/

char	*get_env(char *name, char **env);
int	get_pos_env(char *name, t_env **env);
char	*get_env_instruct(char *name, t_env **env);

/*
**	str_funct.c
*/

int	my_strlen(char *s);
char	*my_strcat_path(char *s1, char *s2);
char	*my_strdup(char *s);
int	my_strcat(char *s1, char *s2);

/*
**	check_funct.c
*/

int	is_and_ope(char *name);
int	is_redirection(char *name);
int	is_command(char *name, char **env);
int	is_binary(char *name);
int	is_builtins(t_builtins **b, char *name);

/*
**	error.c
*/

int	return_error(int ret, char *message);
int	check_error(int e, char **tab);
char	**init_error(void);

/*
**	handle_shell.c
*/

t_env	**init_env(char **env);
int	put_prompt(t_shell *shell, char *prompt);
int	handle_shell(char **env);
int	init_shell(t_shell *shell, char **env);
char	*get_line(char *buff, int n);

/*
**	get_lexem.c
*/

int	get_type(t_shell *shell, char *name, char **env);
int	*get_lexem(t_shell *shell, char **cmd);

/*
**	environ.c
*/

void	display_env(t_env **env);
void	free_env(t_env **env);
char	*get_envline(t_env *env);
char	**get_char_env(t_env **env);
char	*get_struct_env(char *name, t_env **env);

/*
**	exec_line.c
*/

t_sep	**init_sep(t_shell *shell);
int	exec_line(t_shell *shell, char **env);

/*
**	struct_env.c
*/

int	check_env(char *name, t_env **env);

#endif /* MYSH_H_ */
