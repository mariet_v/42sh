/*
** error.h for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sun May  5 19:34:53 2013 Antoine Chiny
** Last update Fri May 24 19:27:05 2013 stephen oudot
*/

#ifndef ERROR_H_
# define ERROR_H_

# define EXIT_SUCESS		0
# define MALLOC_ERROR		-1
# define ENV_ERROR		-2
# define WRITE_ERROR		-3
# define READ_ERROR		-4
# define SYNTAX_ERROR		-5

# define INIT_ERROR		"Can't initialize shell\n"
# define MALLOC_MESSAGE		"Not enough memory\n"
# define ENV_MESSAGE		"Can't get environement\n"
# define WRITE_MESSAGE		"Write error\n"
# define READ_MESSAGE		"\n"
# define SYNTAX_MESSAGE		"Syntax error\n"
# define INVALID_ARG		"Invalid null argument\n"

#endif /* ERROR_H_ */
