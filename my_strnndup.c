/*
** my_strnndup.c for asm in /home/mariet_v//celem/corewar/assembleur/lib
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Sat Feb  2 17:21:20 2013 valentin mariette
** Last update Tue May 21 14:17:14 2013 valentin mariette
*/

#include <stdlib.h>

char	*my_strnndup(char *src, int first, int last)
{
  char	*str;
  int	j;

  j = 0;
  str = malloc(sizeof(char) * (last - first + 2));
  if (str == NULL)
    return (NULL);
  while (src[j] != '\0' && j < (last - first + 1))
    {
      str[j] = src[j + first];
      j++;
    }
  str[last - first + 1] = '\0';
  return (str);
}
