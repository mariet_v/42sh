/*
** get_lexeme.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sat May  4 14:21:28 2013 Antoine Chiny
** Last update Mon May 20 18:01:57 2013 jonathan collinet
*/

#include "mysh.h"

int	get_type(t_shell *shell, char *name, char **env)
{
  if (!is_and_ope(name))
    return (OPE_AND);
  else if (!is_builtins(shell->b_funct, name))
    return (BUILTINS);
  else if (!is_command(name, env))
    return (COMMAND);
  else if (!is_redirection(name))
    return (OPE_PIPE);
  else if (!is_binary(name))
    return (COMMAND);
  else
    return (ARGUMENT);
}

int	*get_lexem(t_shell *shell, char **cmd)
{
  int	i;
  int	*lex;
  char	**env;

  i = -1;
  if (!(env = get_char_env(shell->env)))
    return (NULL);
  if (!(lex = malloc(sizeof(int) * (tablen(cmd) + 1))))
    return (NULL);
  while (cmd[++i])
    lex[i] = get_type(shell, cmd[i], env);
  return (lex);
}
