/*
** cmp_funct.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sun May  5 19:25:31 2013 Antoine Chiny
** Last update Thu May 23 00:09:36 2013 jonathan collinet
*/

#include "mysh.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = -1;
  if (!s1 || !s2)
    return (1);
  if (my_strlen(s1) < n || my_strlen(s1) < n)
    return (1);
  while (++i < n)
    if (s1[i] != s2[i])
      return (1);
  return (0);
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = -1;
  if (my_strlen(s1) != my_strlen(s2))
    return (1);
  while (s1[++i])
    if (s1[i] != s2[i])
      return (1);
  return (0);
}
