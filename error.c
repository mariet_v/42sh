/*
** error.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Tue May  7 01:52:20 2013 Antoine Chiny
** Last update Thu May 16 20:55:21 2013 jonathan collinet
*/

#include "mysh.h"

int	return_error(int ret, char *message)
{
  fprintf(stderr, "%s\n", message);
  return (ret);
}

int	check_error(int e, char **tab)
{
  if (e < 0)
    fprintf(stderr, "%s", tab[ABS(e) - 1]);
  return (-1);
}

char	**init_error(void)
{
  int	i;
  char	**tab;

  i = -1;
  if (!(tab = malloc(sizeof(char *) * 5)))
    return (NULL);
  tab[0] = my_strdup(MALLOC_MESSAGE);
  tab[1] = my_strdup(ENV_MESSAGE);
  tab[2] = my_strdup(WRITE_MESSAGE);
  tab[3] = my_strdup(READ_MESSAGE);
  tab[4] = '\0';
  while (++i < 4)
    if (!tab[i])
      return (NULL);
  return (tab);
}
