/*
** get_next_line.c for g in /home/oudot_s//afs/igraph/wolf3d/lib
**
** Made by stephen oudot
** Login   <oudot_s@epitech.net>
**
** Started on  Wed Dec 12 16:55:42 2012 stephen oudot
** Last update Wed May 22 16:32:16 2013 valentin mariette
*/

#include "get_next_line.h"

void     get_new_buffer(char *ptr, int i)
{
  int    pos;

  pos = 0;
  if (ptr[i] == '\n')
    i = i + 1;
  while (ptr[i])
    {
      ptr[pos] = ptr[i];
      pos = pos + 1;
      i = i + 1;
    }
  while (ptr[pos])
    {
      ptr[pos] = '\0';
      pos = pos + 1;
    }
}

char    *case_big_enough(char *string, char *buffer)
{
  int    i;

  i = 0;
  while (buffer[i] != '\n' && buffer[i] && i < MAX_LEN_BUFFER)
    {
      string[i] = buffer[i];
      i = i + 1;
    }
  string[i] = 0;
  get_new_buffer(buffer, i);
  return (string);
}

char    *case_too_small(char *buf, int fd, int len, int r_v)
{
  int   i;
  char  *str;

  if (!(str = malloc(MAX_LEN_BUFFER)))
    return (0);
  i = 0;
  while (my_strlen(buf) < MAX_LEN_BUFFER && r_v)
    {
      len = len + r_v;
      if ((r_v = read(fd, (buf + len), MAX_LEN_READ)) == -1)
	return (0);
    }
  while (buf[i] != '\n' && buf[i] && i < MAX_LEN_BUFFER)
    {
      str[i] = buf[i];
      i = i + 1;
    }
  str[i] = 0;
  get_new_buffer(buf, i);
  return (str);
}

char		 *get_next_line(const int fd)
{
  static char	 buffer[MAX_LEN_BUFFER];
  static int	 len = 0;
  char		 *string;
  int		 return_value;

  if ((return_value = read(fd, (buffer + len), MAX_LEN_READ)) <= 0 && !len)
    return (0);
  if (!(string = malloc(MAX_LEN_BUFFER)))
    return (0);
  if (MAX_LEN_READ >= MAX_LEN_BUFFER)
    string = case_big_enough(string, buffer);
  else if (MAX_LEN_READ < MAX_LEN_BUFFER)
    string = case_too_small(buffer, fd, len, return_value);
  len = my_strlen(buffer);
  return (string);
}
