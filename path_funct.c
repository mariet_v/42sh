/*
** path_funct.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sun May  5 19:22:12 2013 Antoine Chiny
** Last update Thu May 23 00:28:29 2013 jonathan collinet
*/

#include "mysh.h"

char	*get_env(char *name, char **env)
{
  int	i;

  i = -1;
  while (env[++i])
    if (!my_strncmp(name, env[i], my_strlen(name)))
      return (env[i] + (my_strlen(name) + 1));
  return (NULL);
}

char	*get_env_instruct(char *name, t_env **env)
{
  int	i;

  i = -1;
  if (!env)
    return (NULL);
  while (env[++i])
    if (!my_strncmp(name, env[i]->name, my_strlen(name)))
      return (env[i]->value);
  return (NULL);
}

int	get_pos_env(char *name, t_env **env)
{
  int	i;

  i = -1;
  if (!env)
    return (-1);
  while (env[++i])
    {
      if (!my_strcmp(name, env[i]->name))
	return (i);
    }
  return (-1);
}
