/*
** env_funct.c for 42sh in /home/chiny_a/SVN/42sh-2017-chiny_a
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Wed May 22 15:00:32 2013 Antoine Chiny
** Last update Fri May 24 16:06:03 2013 jonathan collinet
*/

#include "mysh.h"

int	empty_env(t_shell *shell, char *option)
{
  int	i;

  if (my_strcmp(option, "-i"))
    return (return_error(0, "use : env <-i>"));
  i = -1;
  while (shell->env[++i] && shell->env[i]->name)
    del_var_in_env(shell, shell->env[i]->name);
  shell->env = NULL;
  return (return_error(0, "Now the environ is empty."));
}

int	len_env(t_env **env)
{
  int	i;

  i = 0;
  while (env[i])
    i++;
  return (i);
}

int	cp_env(t_env **env, t_shell *shell)
{
  int	i;

  i = -1;
  while (shell->env[++i])
    {
      if (!(env[i] = malloc(sizeof(t_env))))
	return (MALLOC_ERROR);
      env[i]->value = NULL;
      if (!(env[i]->name = my_strdup(shell->env[i]->name)))
	return (MALLOC_ERROR);
      if (shell->env[i]->value)
	if (!(env[i]->value = my_strdup(shell->env[i]->value)))
	  return (MALLOC_ERROR);
    }
  return (0);
}

int	del_var_in_env(t_shell *shell, char *name)
{
  int	i;
  int	j;
  t_env	**env;

  i = -1;
  j = 0;
  if (!(env = malloc(sizeof(t_env *) * len_env(shell->env))))
    return (MALLOC_ERROR);
  while (shell->env[++i])
    if (my_strcmp(shell->env[i]->name, name))
      {
	if (!(env[j] = malloc(sizeof(t_env))))
	  return (MALLOC_ERROR);
	env[j]->value = NULL;
	if (!(env[j]->name = my_strdup(shell->env[i]->name)))
	  return (MALLOC_ERROR);
	if (shell->env[i]->value)
	  if (!(env[j]->value = my_strdup(shell->env[i]->value)))
	    return (MALLOC_ERROR);
	j++;
      }
  env[j] = NULL;
  free_env(shell->env);
  shell->env = env;
  return (0);
}

int	add_var_in_env(t_shell *shell, char *name, char *value)
{
  int	i;
  int	e;
  t_env	**env;

  i = len_env(shell->env);
  if (!(env = malloc(sizeof(t_env *) * (len_env(shell->env) + 2))))
    return (MALLOC_ERROR);
  if ((e = cp_env(env, shell)) < 0)
    return (e);
  if (!(env[i] = malloc(sizeof(t_env))))
    return (MALLOC_ERROR);
  if (!(env[i]->name = my_strdup(name)))
    return (MALLOC_ERROR);
  if (value)
    {
      if (!(env[i]->value = my_strdup(value)))
	return (MALLOC_ERROR);
    }
  else
    env[i]->value = NULL;
  env[i + 1] = NULL;
  free_env(shell->env);
  shell->env = env;
  return (0);
}
