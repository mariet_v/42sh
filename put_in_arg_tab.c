/*
** put_in_arg_tab.c for mysh in /home/mariet_v//celem/mysh1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Dec 18 17:00:08 2012 valentin mariette
** Last update Fri May 24 18:50:20 2013 valentin mariette
*/

#include "mysh.h"

char	**put_in_arg_tab(char *cmd)
{
  char	**tab;
  int	i;
  int	j;
  int	k;

  i = 0;
  j = 0;
  k = 0;
  tab = malloc((my_strlen(cmd) + 1 ) * sizeof(char *));
  if (tab == NULL)
    return (NULL);
  while (cmd[i] != '\0' && cmd[j] != '\0')
    {
      while (cmd[j] != '\0' && cmd[j] != ' ' && cmd[j] != '\t')
	j++;
      i == 0 ? 0 : i++;
      tab[k] = my_strnndup(cmd, i, j - 1);
      k++;
      i = j;
      j++;
    }
  tab[k] = NULL;
  return (tab);
}
