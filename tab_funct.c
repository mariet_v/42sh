/*
** tab_funct.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May  2 17:13:54 2013 Antoine Chiny
** Last update Mon May 20 19:02:24 2013 jonathan collinet
*/

#include "mysh.h"

int	tablen(char **t)
{
  int	i;

  i = -1;
  if (!t)
    return (0);
  while (t[++i]);
  return (i);
}

int	display_tab(char **t)
{
  int	i;

  i = -1;
  while (t[++i])
    printf("%s", t[i]);
  printf("\n");
  return (i);
}

void	free_tab(char **t)
{
  int	i;

  i = -1;
  while (t[++i])
    free(t[i]);
  free(t);
}
