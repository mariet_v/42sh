/*
** exec_funct.c for 42sh in /home/chiny_a/SVN/42sh-2017-chiny_a
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Thu May 16 14:56:03 2013 Antoine Chiny
** Last update Fri May 24 15:57:09 2013 jonathan collinet
*/

#include "mysh.h"

char	*exec_binary(char *cmd, char **tab, t_shell *shell)
{
  char	*path;

  path = get_struct_env("PWD", shell->env);
  if (!(path = my_strcat_path(path, cmd)))
    return (NULL);
  if (!access(path, X_OK))
    {
      free_tab(tab);
      return (path);
    }
  return (NULL);
}

char	*get_exec_path(char *cmd, int type, t_shell *shell, int *e)
{
  int	i;
  char	**tab;
  char	*path;

  i = -1;
  if (!(check_env("PATH", shell->env)))
    {
      if (!(tab = my_wordtab(get_struct_env("PATH", shell->env), ":")))
	return (NULL);
      if (type == BINARY)
	return (exec_binary((cmd += 2), tab, shell));
      while (tab[++i])
	{
	  if (!(path = my_strcat_path(tab[i], cmd)))
	    return (NULL);
	  if (!access(path, X_OK))
	    {
	      free_tab(tab);
	      return (path);
	    }
	}
      free_tab(tab);
      free(path);
    }
  *e = -1;
  return (NULL);
}

int	get_nbr_args(int id, t_sep *sep)
{
  int	i;

  i = 0;
  while (sep->cmd[id] && sep->type[id] == ARGUMENT)
    {
      i++;
      id++;
    }
  return (i);
}

char	**get_cmd_args(int id, t_sep *sep)
{
  int	i;
  char	**args;

  i = 0;
  if (!(args = malloc(sizeof(char *) * (get_nbr_args(id, sep) + 2))))
    return (NULL);
  args[i++] = sep->cmd[id - 1];
  while (sep->cmd[id] && sep->type[id] == ARGUMENT)
    args[i++] = sep->cmd[id++];
  args[i] = NULL;
  return (args);
}

int	exec_path_cmd(char *cmd, char **args, char **env)
{
  pid_t	pid;

  if ((pid = fork()) == -1)
    return (-1);
  if (!pid)
    {
      execve(cmd, args, env);
      printf("Command not found\n");
    }
  else
    wait(0);
  return (0);
}

int	exec_cmd(int id, t_shell *shell, t_sep *sep)
{
  int	e;
  char	*path;
  char	**env;
  char	**args;

  e = 0;
  args = NULL;
  if (!(env = get_char_env(shell->env)))
    return (MALLOC_ERROR);
  if (!(args = get_cmd_args((id + 1), sep)))
    return (MALLOC_ERROR);
  if ((path = get_exec_path(sep->cmd[id], sep->type[id], shell, &e)))
    {
      if (e)
	return (-1);
      if (exec_path_cmd(path, args, env) == -1)
	return (-1);
    }
  else
    return (-1);
  return (tablen(sep->cmd));
}
