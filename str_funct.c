/*
** str_funct.c for 42sh in /home/chiny_a/42sh
**
** Made by Antoine Chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Sun May  5 19:41:48 2013 Antoine Chiny
** Last update Thu May 23 05:13:56 2013 jonathan collinet
*/

#include "mysh.h"

int	my_strlen(char *s)
{
  int	i;

  i = -1;
  if (!s)
    return (0);
  while (s[++i]);
  return (i);
}

char	*my_strcat_path(char *s1, char *s2)
{
  int	i;
  int	j;
  char	*ret;

  i = 0;
  j = 0;
  if ((!s1 && !s2))
    return (NULL);
  if ((ret = malloc(sizeof(char)
		    * (my_strlen(s1) + my_strlen(s2) + 2))) == NULL)
    return (NULL);
  if (s1)
    while (s1[i])
      {
	ret[i] = s1[i];
	i++;
      }
  ret[i++] = '/';
  if (s2)
    while (s2[j])
      ret[i++] = s2[j++];
  ret[i] = '\0';
  return (ret);
}

char	*my_strdup(char *s)
{
  int	i;
  char	*r;

  i = -1;
  if (!(r = malloc(sizeof(char) * (my_strlen(s) + 1))))
    return (NULL);
  while (s[++i])
    r[i] = s[i];
  r[i] = '\0';
  return (r);
}

int	my_strcat(char *s1, char *s2)
{
  int	i;
  int	j;

  if (!(s1 = realloc(s1, sizeof(char) * my_strlen(s1) + my_strlen(s2) + 1)))
    return (MALLOC_ERROR);
  i = -1;
  j = my_strlen(s1);
  while (s2[++i])
    {
      s1[j] = s2[i];
      j++;
    }
  s1[j] = '\0';
  return (0);
}

