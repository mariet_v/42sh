/*
** builtins.c for 42sh in /home/chiny_a//42sh
**
** Made by antoine chiny
** Login   <chiny_a@epitech.net>
**
** Started on  Wed May  8 13:55:37 2013 antoine chiny
** Last update Thu May 23 00:04:39 2013 jonathan collinet
*/

#include "mysh.h"

int	my_setenv(t_shell *shell, char **cmd)
{
  if (tablen(cmd) != 3)
    return (return_error(0, "use : setenv \"var\" \"value\""));
  if (get_env_instruct(cmd[1], shell->env))
    return (return_error(0, "setenv : var is already exist."));
  if (!cmd[1] || !cmd[2])
    return (return_error(0, "setenv : var or value is null."));
  return (add_var_in_env(shell, cmd[1], cmd[2]));
}

int	my_unsetenv(t_shell *shell, char **cmd)
{
  if (tablen(cmd) != 2)
    return (return_error(0, "use : unsetenv \"var\""));
  if (!get_env_instruct(cmd[1], shell->env))
    return (return_error(0, "unsetenv : var isn't exist."));
  if (!cmd[1])
    return (return_error(0, "unsetenv : var is null."));
  return (del_var_in_env(shell, cmd[1]));
}

int	my_cd(t_shell *shell, char **cmd)
{
  int	pos;
  char	*pwd;
  char	**cmd_path;

  if ((pos = get_pos_env("PWD", shell->env)) == -1)
    return (return_error(0, "cd : cannot find PWD in env."));
  pwd = get_env_instruct("PWD", shell->env);
  if (tablen(cmd) > 2)
    return (return_error(0, "cd : Too much arguments."));
  if (!(cmd_path = my_wordtab(cmd[1], "/")))
    return (go_here(shell->env, get_env_instruct("HOME", shell->env)));
  if (cmd[1][0] == '/')
    return (build_new_pwd(shell, NULL, cmd_path, -1));
  if (!strcmp(cmd[1], "-"))
    return (old_pwd(shell, pos));
  if (!strcmp(cmd_path[0], "~"))
    {
      if (!get_env_instruct("HOME", shell->env))
  	return (return_error(0, "cd : cannot find HOME in env."));
      pwd = my_strdup(get_env_instruct("HOME", shell->env));
      cmd_path++;
    }
  return (build_new_pwd(shell, my_wordtab(pwd, "/"), cmd_path, -1));
}

int	my_env(t_shell *shell, char **cmd)
{
  if (tablen(cmd) > 2)
    return (return_error(0, "use : env <-i>"));
  if (tablen(cmd) == 2)
    return (empty_env(shell, cmd[1]));
  if (cmd)
    display_env(shell->env);
  return (0);
}

int	my_exit(t_shell *shell, char **cmd)
{
  if (tablen(cmd) != 1)
    return (return_error(0, "exit don't take arguments..."));
  if (shell->old_pwd)
    free(shell->old_pwd);
  free_tab(cmd);
  return (-42);
}
