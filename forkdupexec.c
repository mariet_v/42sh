/*
** forkdupexec.c for mysh in /home/mariet_v//celem/mysh
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Mar  7 16:26:06 2013 valentin mariette
** Last update Sat May 25 14:48:11 2013 valentin mariette
*/

#include "mysh.h"

int		forkdupexec(t_listecmd *liste, char **env)
{
  t_listecmd	*tmp;
  char		**arg_tab;
  int		fils;
  int		status;

  tmp = liste;
  my_check_env(env);
  while (tmp != NULL)
    {
      if (tmp->buff)
	arg_tab = arg_tab_double_left(tmp->cmd);
      arg_tab = put_in_arg_tab(tmp->cmd);
      fils = fork();
      if (!fils)
	{
	  if ((dup2(tmp->in, 0) == -1) || (dup2(tmp->out, 1) == -1))
	    return (-1);
	  if (tmp->buff)
	    {
	      dup2(tmp->double_out, 0);
	      close(tmp->double_in);
	    }
	  exec_local_bin(arg_tab, env);
	  execve(get_concatenate_path(arg_tab[0], my_check_env(env)), arg_tab, env);
	}
      if (tmp->buff)
	{
	  close(tmp->double_out);
	  write(tmp->double_in, tmp->buff, my_strlen(tmp->buff));
	  close(tmp->double_in);
	}
      tmp = get_next(tmp);
    }
  waitpid(fils, &status, 0);
  look_status(status);
  return (0);
}

t_listecmd	*get_next(t_listecmd *tmp)
{
  if (tmp->in != 0)
    close(tmp->in);
  if (tmp->out != 1)
    close(tmp->out);
  tmp = tmp->next;
  return (tmp);
}

char	*my_check_env(char **env)
{
  int	i;

  i = 0;
  while (env[i] != NULL && my_strncmp(env[i], "PATH", 4) != 0)
    i++;
  if (env[i] == NULL)
    return (NULL);
  return (env[i]);
}

char **arg_tab_double_left(char *cmd)
{
  char	**tab;
  
  if (!(tab = malloc(sizeof(char *) * 2)))
    return (NULL);
  tab[0] = my_strdup(cmd);
  tab[1] = NULL;
  return (tab);
}
