/*
** look_status.c for look_status in /home/mariet_v//celem/42sh/42sh
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri May 24 18:49:10 2013 valentin mariette
** Last update Fri May 24 18:49:15 2013 valentin mariette
*/

#include "mysh.h"

void	look_status(int status)
{
  if (status == 11)
    my_putstr("Segmentation fault\n");
  else if (status == 8)
    my_putstr("Floating exception\n");
  else if (status == 4)
    my_putstr("Illegal instruction\n");
  else if (status == 16)
    my_putstr("Stack fault\n");
  else if (status != 0)
    my_putstr("Program exited with errors\n");
}
